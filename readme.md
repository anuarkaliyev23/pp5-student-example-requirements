# Технические требования к реализации задания для ПП-5

![Изображение Доски](board-photo.jpg)

## Структура Базы данных

### Student

|  Колонка   |   Тип   |      Описание      |
|:----------:|:-------:|:------------------:|
|     id     | INTEGER |    PRIMARY KEY     |
| first_name | STRING  |                    |
| last_name  | STRING  |                    |
|   phone    | STRING  |                    |
|   email    | STRING  |                    |
|  group_id  | INTEGER | FOREIGN KEY(Group) |

### Student_Group

|    Колонка     |   Тип   |  Описание   |
|:--------------:|:-------:|:-----------:|
|       id       | INTEGER | PRIMARY KEY |
|      name      | STRING  |             |
| specialty_name | STRING  |             |

## Запросы

### Список всех групп

#### Запрос

`GET` /groups

#### Ответ

Status - 200

```json
    [
        {
            "id" : 1,
            "name" : "CS-1",
            "specialty_name" : "Computer Science",
            "students": [
                {
                    "id" : 1,
                    "first_name": "John",
                    "last_name" : "Doe",
                    "phone" : "+7 777 77 77 777",
                    "email" : "john.doe@example.com",
                    "group" : "CS-1"
                },
                {
                    "id" : 3,
                    "first_name": "Mary",
                    "last_name" : "Jane",
                    "phone" : "+7 777 43 90 342",
                    "email" : "mary.jane@example.com",
                    "group" : "CS-1"
                }
            ]
        },
        {
            "id" : 2,
            "name" : "CS-2",
            "specialty_name" : "Computer Science",
            "students": [
                {
                    "id" : 4,
                    "first_name": "James",
                    "last_name" : "Doe",
                    "phone" : "+7 777 77 77 777",
                    "email" : "james.doe@example.com",
                    "group" : "CS-2"
                },
                {
                    "id" : 5,
                    "first_name": "Juliana",
                    "last_name" : "Vanger",
                    "phone" : "+7 777 43 90 342",
                    "email" : "juliana.vanger@example.com",
                    "group" : "CS-2"
                }
            ]
        },
        {
            "id" : 3,
            "name" : "H-1",
            "specialty_name" : "History",
            "students": [
                {
                    "id" : 6,
                    "first_name": "August",
                    "last_name" : "Heims",
                    "phone" : "+7 777 77 77 777",
                    "email" : "august.heims@example.com",
                    "group" : "CS-2"
                }
            ]
        }
    ]
```

### Информация об одной группе

#### Запрос

`GET` /groups/1

#### Ответ

Status - 200

```json
    {
        "id" : 1,
        "name" : "CS-1",
        "specialty_name" : "Computer Science",
        "students": [
            {
                "id" : 1,
                "first_name": "John",
                "last_name" : "Doe",
                "phone" : "+7 777 77 77 777",
                "email" : "john.doe@example.com",
                "group" : "CS-1"
            },
            {
                "id" : 3,
                "first_name": "Mary",
                "last_name" : "Jane",
                "phone" : "+7 777 43 90 342",
                "email" : "mary.jane@example.com",
                "group" : "CS-1"
            }
        ]
    }
```

### Создание группы

#### Запрос

`POST` /groups

```json
    {
        "id" : 3,
        "name": "CS-2",
        "specialty_name" : "Computer Science"
    }
```

#### Ответ

Status - 201

```json
    {
        "id" : 3,
        "name" : "CS-2",
        "specialty_name" : "Computer Science",
        "students": []
    }
```

### Редактирование группы

#### Запрос

`PATCH` /groups/3

```json
    {
        "id" : 3,
        "name": "CS-5",
        "specialty_name" : "Computer Science"
    }
```

#### Ответ

Status - 200

```json
    {
        "id" : 3,
        "name" : "CS-5",
        "specialty_name" : "Computer Science",
        "students": []
    }
```

### Удаление группы группы

#### Запрос

`DELETE` /groups/3

```json
    {
        "id" : 3,
        "name": "CS-5",
        "specialty_name" : "Computer Science"
    }
```

#### Ответ

Status - 204

### Список студентов

#### Запрос

`GET` /students

#### Ответ

Status - 200

```json
    [
        {
            "id" : 1,
            "first_name": "John",
            "last_name" : "Doe",
            "phone" : "+7 777 77 77 777",
            "email" : "john.doe@example.com",
            "group" : "CS-1"
        },
        {
            "id" : 2,
            "first_name": "Jane",
            "last_name" : "Doe",
            "phone" : "+7 777 11 11 111",
            "email" : "jane.doe@example.com",
            "group" : "H-1"
        }
    ]
```

### Информация о студенте

#### Запрос

`GET` /students/1

#### Ответ

Status - 200

```json
    {
        "id" : 1,
        "first_name": "John",
        "last_name" : "Doe",
        "phone" : "+7 777 77 77 777",
        "email" : "john.doe@example.com",
        "group" : "CS-1"
    }
    
```

### Создание студента

#### Запрос

`POST` /students

```json
    {
        "id" : 1,
        "first_name": "John",
        "last_name" : "Doe",
        "phone" : "+7 777 77 77 777",
        "email" : "john.doe@example.com",
        "group" : 1
    }
```

#### Ответ

Status - 201

```json
    {
        "id" : 1,
        "first_name": "John",
        "last_name" : "Doe",
        "phone" : "+7 777 77 77 777",
        "email" : "john.doe@example.com",
        "group" : "CS-1"
    }
```

### Редактирование студента

#### Запрос

`PATCH` /students/1

```json
    {
        "id" : 1,
        "first_name": "John",
        "last_name" : "Smith",
        "phone" : "+7 777 77 77 777",
        "email" : "john.doe@example.com",
        "group" : 1
    }
```

#### Ответ

Status - 200

```json
    {
        "id" : 1,
        "first_name": "John",
        "last_name" : "Smith",
        "phone" : "+7 777 77 77 777",
        "email" : "john.doe@example.com",
        "group" : "CS-1"
    }
```

### Удаление студента

#### Запрос

`DELETE` /students/1

#### Ответ

Status - 204

> В случае любой ошибки (например удаление группы, в которой есть студены, сервер должен отдавать 4хх статус код
> 